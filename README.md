# Pixms/DataTables component

This library is designed to take away the pain from building tables. The library generates an array of data that can be parsed by your favorite template renderer to build a table.

## Dependencies

Absolutely none.

## How to use

```

<?php

use Pixms\DataTables\DataTable;
use Pixms\DataTables\DataTableAction;
use Pixms\DataTables\DataTableFactoryInterface;

class MyDataTableFactory implements DataTableFactoryInterface
{

    public function create($data)
    {
        $table = new DataTable($data);
        $table->addColumns(['name' => 'Name', 'email' => 'E-mail', 'role' => 'Role', 'status' => 'Status'])
                ->addAction(new DataTableAction('edit', 'Edit', array($this, 'editUrl')))
                ->addAction(new DataTableAction('cancel-circle', 'Delete', array($this, 'deleteUrl')));

        return $table;
    }
    public function editUrl($row)
    {
        return '/users/details/' . $row->id;
    }

    public function deleteUrl($row)
    {
        return '/users/delete/' . $row->id;
    }

}

//In your controller

$factory = new MyDataTableFactory();
$table = $factory->create([]);

```

## Todos

* Provide a PHP file that actually generate the HTML
* Provide a Mustache that actually generate the HTML
* A working example