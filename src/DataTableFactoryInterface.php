<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\DataTables;

/**
 *
 * @author Sonia
 */
interface DataTableFactoryInterface
{
    public function create($data);
}
