<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\DataTables;

/**
 * Description of DataTableConfig
 * http://datatables.net/reference/option/
 *
 * @author Sonia
 */
class DataTableConfig
{
    /**
     * Feature control the end user's ability to change the paging display length of the table.
     */
    protected $lengthChange = true;
    
    /**
     * Feature control ordering (sorting) abilities in DataTables.
     * @var bool 
     */
    protected $ordering = true;
    
    /**
     * Enable or disable table pagination.
     * @var bool 
     */
    protected $paging = true;
    
    /**
     * Feature control search (filtering) abilities
     * @var bool 
     */
    protected $searching = true;
    
    /**
     * Feature control DataTables' server-side processing mode.
     * @var bool 
     */
    protected $serverSide = false;
    
    /**
     * Set the URL from where the data should be loaded from.
     * @var string 
     */
    protected $ajax;
    
    /**
     * State saving - restore table state on page reload
     * @var bool 
     */
    protected $stateSave = false;
    
    /**
     * Click and drag column headers to reorder a table
     * @var bool 
     */
    protected $reorderColumns = false;
    
    /**
     * Dynamically change the visibility of the columns in the table
     * @var bool 
     */
    protected $displayColumns = false;
    
    /**
     * Dynamically change the visibility of the columns in the table
     * @var bool 
     */
    protected $fixedColumns = false;
    
    /**
     * Dynamically change the visibility of the columns in the table
     * @var bool 
     */
    protected $fixedHeader = false;
    
    /**
     * Change the options in the page length select list.
     * @var array 
     */
    protected $lengthMenu = [10, 25, 50, 100];
    
    /**
     * Initial order (sort) to apply to the table
     * @var array 
     */
    protected $order = [[0, 'asc']];
    
    /**
     * Ordering to always be applied to the table
     * @var array 
     */
    protected $orderFixed = [0, 'asc'];
    
    /**
     * Change the initial page length (number of rows per page)
     * @var int 
     */
    protected $pageLength = 10;
    /**
     * Language lines
     * @var array 
     */
    protected $language = null;

    
    public function __construct($config = array())
    {
        $this->set($config);
    }

    public function set($config)
    {
        if (!is_array($config) && !$config instanceof Traversable && !$config instanceof stdClass) {
            throw new InvalidArgumentException("The configuration must be traversable.");
        }

        foreach ($config as $key => $value) {
            $property = $this->snakeToCamelCase($key);

            if (property_exists($this, $property)) {
                $this->$property = $value;
            } else {
                throw new OutOfBoundsException("The configuration key " . $key . " does not exist.");
            }
        }
    }

    public function get($config_key)
    {
        $property = $this->snakeToCamelCase($config_key);
        
        if (property_exists($this, $property)) {
            return $this->$property;
        } else {
            throw new OutOfBoundsException("The configuration key " . $config_key . " does not exist.");
        }
    }

    public function getAll()
    {
        $config = array();

        foreach ($this as $property => $value) {
            $config[$this->snakeToCamelCase($property)] = $value;
        }

        return $config;
    }
    
    public function __toString()
    {
        return json_encode($this->getAll());
    }
    
    private function camelToSnakeCase($str)
    {
        return ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $str)), '_');
    }

    private function snakeToCamelCase($str)
    {
        $closure = function($matches) {
            return strtoupper($matches[1]);
        };
        return preg_replace_callback('/_([a-z])/', $closure, $str);
    }
}
