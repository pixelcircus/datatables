<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\DataTables;

/**
 * Description of DataTableColumn
 *
 * @author Sonia
 */
class DataTableColumn
{

    protected $classes = array();
    protected $property;
    protected $label;
    protected $visible = true;
    protected $orderable = true;
    protected $searchable = true;
    protected $type;
    protected $value;

    public function __construct($property, $label, $classes = array(), $visible = true, $orderable = true, $searchable = true, $type = null)
    {
        $this->property = $property;
        $this->label = $label;
        $this->classes = $classes;
        $this->visible = $visible;
        $this->orderable = $orderable;
        $this->searchable = $searchable;
        $this->type = $type;
    }

    public function set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
            return $this;
        }

        throw new \OutOfBoundsException('The property "' . $property . '" does not exist.');
    }

    public function getClasses()
    {
        return $this->classes;
    }

    public function classes()
    {
        return implode(' ', $this->classes);
    }

    public function getProperty()
    {
        return $this->property;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getVisible()
    {
        return $this->visible;
    }

    public function getOrderable()
    {
        return $this->orderable;
    }

    public function getSearchable()
    {
        return $this->searchable;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

}
