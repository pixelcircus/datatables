<?php

namespace Pixms\DataTables;

class DataIterator implements \Iterator
{
    protected $data;
    protected $columns;
    protected $actions;

    protected $position = 0;

    public function __construct($data, $columns, $actions)
    {
        $this->data = $data;
        $this->columns = $columns;
        $this->actions = $actions;
    }
    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return array('columns' => new ColumnsIterator($this->data[$this->position], $this->columns), 'actions' => $this->getActions());
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->data[$this->position]);
    }

    public function getActions()
    {
        foreach ($this->actions as $action) {
            $action->setUrl($this->data[$this->position]);
        }

        return $this->actions;
    }

}
