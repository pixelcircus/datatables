<?php

namespace Pixms\DataTables;

class ColumnsIterator implements \Iterator
{

    protected $data;
    protected $columns;
    protected $position = 0;

    public function __construct($data, $columns)
    {
        $this->data = $data;
        $this->columns = array_values($columns);
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        $column = $this->columns[$this->position];
        $property = $column->getProperty();
        switch (gettype($this->data)) {
            case 'object':
                if (!($this->data instanceof Closure)) {
                    // Note that is_callable() *will not work here*
                    // See https://github.com/bobthecow/mustache.php/wiki/Magic-Methods
                    if (method_exists($this->data, $property)) {
                        $column->setValue($this->data->$property());
                    } else if (isset($this->data->$property)) {
                        $column->setValue($this->data->$property);
                    } else if ($this->data instanceof ArrayAccess && isset($this->data[$property])) {
                        $column->setValue($this->data[$property]);
                    }
                }
                break;
            case 'array':
                if (array_key_exists($property, $this->data)) {
                    $column->setValue($this->data[$property]);
                }
                break;
        }

        return $column;
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->columns[$this->position]);
    }

}
