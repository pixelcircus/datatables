<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\DataTables;

/**
 * Description of DataTableAction
 *
 * @author Sonia
 */
class DataTableAction
{

    protected $label;
    protected $action;
    protected $callable;
    protected $url = '#';

    public function __construct($action, $label, callable $callable)
    {
        $this->label = $label;
        $this->action = $action;
        $this->callable = $callable;
    }

    public function label()
    {
        return $this->label;
    }

    public function action()
    {
        return $this->action;
    }

    public function url()
    {
        return $this->url;
    }

    public function setUrl($row)
    {
        $this->url = call_user_func_array($this->callable, [$row]);
        return $this;
    }

}
