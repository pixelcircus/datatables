<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\DataTables;

/**
 * Description of DataTable
 *
 * @author Sonia
 */
class DataTable
{

    protected $id;
    protected $attributes;
    protected $data;
    protected $config;
    protected $actions = array();
    protected $columns = array();
    protected static $global_config;

    public function __construct($data = null, DataTableConfig $config = null, $id = null, $attributes = array())
    {
        $this->data = $data;
        if (is_null($id)) {
            $id = uniqid('datatable');
        }
        $this->id = $id;
        $this->attributes = $attributes;
        
        
        $this->config = $config ? : self::getGlobalConfig();
    }
    
    public static function getGlobalConfig() {
        if (is_null(self::$global_config)) {
            self::$global_config = new DataTableConfig;
        }
        
        return self::$global_config;
    }
    
    public static function setGlobalConfig(DataTableConfig $config) {
        self::$global_config = $config;
    }

    public function addAction(DataTableAction $action)
    {
        $this->actions[$action->action()] = $action;
        return $this;
    }

    public function addColumn(DataTableColumn $column)
    {
        $this->columns[$column->getProperty()] = $column;
        return $this;
    }

    public function addColumns($columns)
    {
        foreach ($columns as $property => $label) {
            $this->addColumn(new DataTableColumn($property, $label));
        }

        return $this;
    }

    public function hideColumns($column_names)
    {
        $this->setColumns($column_names, 'visible', false);

        return $this;
    }
    
    public function addClasses($column, $classes = [])
    {
        $this->setColumns($column, 'classes', $classes);

        return $this;
    }
    
    public function setColumns($column_names, $property, $value)
    {
        
        if (is_string($column_names)) {
            $column_names = array($column_names);
        }
        
        foreach ($column_names as $name) {
            if (isset($this->columns[$name])) {
                $this->columns[$name]->set($property, $value);
            } else {
                throw new OutOfBoundsException("The column '" . $name . "' does not exist.");
            }
        }

        return $this;
    }
    
    public function exportViewData() {
        return array(
            'id' => $this->id,
            'attributes' => $this->attributes,
            'columns' => $this->columns,
            'data' => $this->data,
            'config' => $this->config,
            'actions' => $this->actions
        );
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getConfig()
    {
        return $this->config;
    }
    
    public function config()
    {
        $config = $this->config->getAll();
        $config['columns'] = [];
        foreach ($this->columns as $column) {
            $config['columns'][] = [
                'data' => $column->getProperty(),
                'className' => $column->getProperty().' '.implode(' ', $column->getClasses()),
                'visible' => $column->getVisible(),
                'orderable' => $column->getOrderable(),
                'searchable' => $column->getSearchable(),
                'type' => $column->getType()
            ];
        }
        if ($this->hasActions()) {
            $config['columns'][] = [
                'data' => 'actions',
                'className' => 'actions',
                'visible' => true,
                'orderable' => false,
                'searchable' => false
            ];
        }
        return json_encode($config);
    }

    public function getActions()
    {
        return $this->actions;
    }
    public function actions()
    {
        return array_values($this->actions);
    }
    public function hasActions()
    {
        return (bool) count($this->actions);
    }

    public function getColumns()
    {
        return array_values($this->columns);
    }

    public function columns()
    {
        return $this->columns;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function setConfig($config)
    {
        $this->config = $config;
        return $this;
    }
    
    public function setConfigProperty($property, $value)
    {
        $this->config->set(array($property => $value));
        return $this;
    }

    public function rows()
    {
        return new DataIterator($this->data, $this->columns, $this->actions());
    }
    
    public function attributes() 
    {
        $attribute_str = ' ';
        $this->attributes['id'] = $this->getId();
        foreach ($this->attributes as $attribute => $value) {
            $attribute_str .= $attribute.'="'.  htmlspecialchars((string)$value, ENT_QUOTES).'" ';
        }

        return $attribute_str;
    }



}
